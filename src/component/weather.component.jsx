import React, { Component } from 'react';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import axios from 'axios';

class Weather extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
  }

  componentDidMount() {
    axios
      .get(`http://api/weatherforecast`)
      .then((res) => {
        this.setState({ list: res.data });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const list = this.state.list;
    return (
      <div>
        {list &&
          list.map((item) => (
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>{item.summary}</Card.Title>
                <Card.Text>{item.date}</Card.Text>
              </Card.Body>
              <ListGroup className="list-group-flush">
                <ListGroupItem>{'Celcius: ' + item.temperatureC}</ListGroupItem>
                <ListGroupItem>
                  {'Farenheit: ' + item.temperatureF}
                </ListGroupItem>
              </ListGroup>
            </Card>
          ))}
      </div>
    );
  }
}

export default Weather;
