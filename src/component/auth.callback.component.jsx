/* /src/components/auth/callback.jsx */
import React from 'react';
import { AuthConsumer } from '../Auth/authProvider.jsx';

export const Callback = () => (
  <AuthConsumer>
    {({ signinRedirectCallback }) => {
      signinRedirectCallback();
      return <span>loading</span>;
    }}
  </AuthConsumer>
);
