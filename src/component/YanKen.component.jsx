import React, { Component } from 'react';
import axios from 'axios';
import { Card, Button } from 'react-bootstrap';

class YanKen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
    };

    this.refreshGame = this.refreshGame.bind(this);
    this.playYanKenPon = this.playYanKenPon.bind(this);
  }

  componentDidMount() {}
  playYanKenPon(index) {
    axios
      .get(`http://api/api/YanKen/` + index)
      .then((res) => {
        this.setState({ result: res.data });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshGame() {
    this.setState({ result: null });
  }

  render() {
    const result = this.state.result;
    return (
      <div>
        <Card className="text-center">
          <Card.Header>Yan Ken Pon</Card.Header>
          <Card.Body>
            <Card.Title>Mini Juego</Card.Title>
            <Card.Text>ELIJA SU MANO PARA JUGAR</Card.Text>
            {!result && (
              <div>
                <Button
                  className="m-2"
                  variant="primary"
                  onClick={() => this.playYanKenPon(0)}
                >
                  PIEDRA
                </Button>
                <Button
                  className="m-2"
                  variant="primary"
                  onClick={() => this.playYanKenPon(1)}
                >
                  PAPEL
                </Button>
                <Button
                  className="m-2"
                  variant="primary"
                  onClick={() => this.playYanKenPon(2)}
                >
                  TIJERA
                </Button>
              </div>
            )}
          </Card.Body>
        </Card>
        {result && (
          <Card className="text-center">
            <Card.Header>PUM</Card.Header>
            <Card.Body>
              <Card.Title>{result}</Card.Title>
              <Card.Text>
                <Button
                  className="m-2"
                  variant="info"
                  onClick={() => this.refreshGame()}
                >
                  Volver a Jugar
                </Button>
              </Card.Text>
            </Card.Body>
          </Card>
        )}
      </div>
    );
  }
}

export default YanKen;
