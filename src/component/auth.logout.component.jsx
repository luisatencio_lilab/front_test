import React from 'react';
import { AuthConsumer } from '../Auth/authProvider.jsx';

export const Logout = () => (
  <AuthConsumer>
    {({ logout }) => {
      logout();
      return <span>loading</span>;
    }}
  </AuthConsumer>
);
