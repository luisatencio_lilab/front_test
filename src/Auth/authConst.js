const _auth_server = 'https://aaea467a4328.ngrok.io';

export const IDENTITY_CONFIG = {
  authority: _auth_server, //(string): The URL of the OIDC provider.
  client_id: 'js', //(string): Your client application's identifier as registered with the OIDC provider.
  redirect_uri: 'https://localhost:3000', //The URI of your client application to receive a response from the OIDC provider.
  login: _auth_server + '//account/login',
  automaticSilentRenew: false, //(boolean, default: false): Flag to indicate if there should be an automatic attempt to renew the access token prior to its expiration.
  loadUserInfo: false, //(boolean, default: true): Flag to control if additional identity data is loaded from the user info endpoint in order to populate the user's profile.
  // silent_redirect_uri: process.env.REACT_APP_SILENT_REDIRECT_URL, //(string): The URL for the page containing the code handling the silent renew.
  // post_logout_redirect_uri: process.env.REACT_APP_LOGOFF_REDIRECT_URL, // (string): The OIDC post-logout redirect URI.
  // audience: 'https://example.com', //is there a way to specific the audience when making the jwt
  responseType: 'code', //(string, default: 'id_token'): The type of response desired from the OIDC provider.
  grantType: 'implicit',
  scope: 'openid api1', //(string, default: 'openid'): The scope being requested from the OIDC provider.
  // webAuthResponseType: 'id_token token',
};

export const METADATA_OIDC = {
  issuer: _auth_server,
  jwks_uri: _auth_server + '/.well-known/openid-configuration/jwks',
  authorization_endpoint: _auth_server + '/connect/authorize',
  token_endpoint: _auth_server + '/connect/token',
  userinfo_endpoint: _auth_server + '/connect/userinfo',
  end_session_endpoint: _auth_server + '/connect/endsession',
  check_session_iframe: _auth_server + '/connect/checksession',
  revocation_endpoint: _auth_server + '/connect/revocation',
  introspection_endpoint: _auth_server + '/connect/introspect',
};
