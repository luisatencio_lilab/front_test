import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Wheater from './component/weather.component';
import YanKen from './component/YanKen.component';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="navbar navbar-expand navbar-dark bg-dark">
            <h1 className="navbar-brand">Test CI</h1>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={'/Wheater'} className="nav-link">
                  Wheater
                </Link>
              </li>
              <li className="nav-item">
                <Link to={'/YanKen'} className="nav-link">
                  YanKenPonGame
                </Link>
              </li>
            </div>
          </div>
          <div className="container mt-3">
            <Switch>
              <Route exact path={['/', '/Wheater']} component={Wheater} />
              <Route exact path="/YanKen" component={YanKen} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
