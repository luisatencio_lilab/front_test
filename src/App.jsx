import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Wheater from './component/weather.component';
import YanKen from './component/YanKen.component';

import { Callback } from './component/auth.callback.component.jsx';
import { Logout } from './component/auth.logout.component.jsx';
import { LogoutCallback } from './component/auth.logoutCallback.component.jsx';
import { SilentRenew } from './component/auth.silentRenew.component.jsx';

import { PrivateRoute } from './component/PrivateRoute';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="navbar navbar-expand navbar-dark bg-dark">
            <h1 className="navbar-brand">Test</h1>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={'/Wheater'} className="nav-link">
                  Wheater
                </Link>
              </li>
              <li className="nav-item">
                <Link to={'/YanKen'} className="nav-link">
                  YanKenPonGame
                </Link>
              </li>
            </div>
          </div>
          <div className="container mt-3">
            <Switch>
              <Route exact={true} path="/signin-oidc" component={Callback} />
              <Route exact={true} path="/logout" component={Logout} />
              <Route
                exact={true}
                path="/logout/callback"
                component={LogoutCallback}
              />
              <Route exact={true} path="/silentrenew" component={SilentRenew} />
              <PrivateRoute path={['/', '/Wheater']} component={Wheater} />
              <PrivateRoute path="/YanKen" component={YanKen} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
